# OpenMW Lua examples

This repo contains several simple OpenMW mods.
They were developed as [OpenMW Lua](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/index.html) examples.

*"Fast equip"*

Provides hotkeys to switch between several equipment sets.

Controls:

* 'M' -- save current equipment
* '<', '>' -- iterate over saved equipment sets
* '/' -- remove current equipment set from the list

*"Skooma effect"*

Affects camera and player movement after drinking skooma.

*"Free camera"*

Simply adds free camera mode.

Controls:

* 'O' key -- toggle free camera mode
* Toggle POV (TAB by default) in free camera mode -- switch between camera controls/player controls
* WASD - move camera
* Mouse wheel - move camera vertically

## Installation

Just download `DataFiles` from this repo and add some of these lines to `openmw.cfg`:

```
data="path/to/DataFiles"  # specify the download location here

content=fast_equip.omwscripts
content=skooma_effect.omwscripts
content=free_camera.omwscripts
```
