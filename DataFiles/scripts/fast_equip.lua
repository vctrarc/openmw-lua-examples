local self = require('openmw.self')
local ui = require('openmw.ui')

local sets = {}
local index = 0

local function onKeyPress(key)
    if key.symbol == 'm' then
        index = index + 1
        for i = #sets, index, -1 do sets[i + 1] = sets[i] end
        eqp = {}
        for slot, item in pairs(self:getEquipment()) do
            eqp[slot] = item.recordId
        end
        sets[index] = eqp
        ui.showMessage(string.format('New equipment set. Use \'<\' and \'>\' to iterate over %d sets.', #sets))
    end
    if key.symbol == '/' then
        if #sets == 0 then
            ui.showMessage('No equipment sets. Press \'M\' to save current equipment.')
            return
        end
        for i = index, #sets do sets[i] = sets[i + 1] end
        ui.showMessage(string.format('Equipment set %d removed. %s sets left.', index, #sets))
        if index > #sets then index = #sets end
    end
    if key.symbol == ',' or key.symbol == '.' then
        if #sets == 0 then
            ui.showMessage('No equipment sets. Press \'M\' to save current equipment.')
            return
        end
        if key.symbol == ',' then
            index = index - 1
        else
            index = index + 1
        end
        if index > #sets then index = 1 end
        if index < 1 then index = #sets end
        self:setEquipment(sets[index])
        ui.showMessage(string.format("Current equipment set: %d of %d", index, #sets))
    end
end

return {
    engineHandlers = {
        onKeyPress = onKeyPress,
        onLoad = function(data) sets, index = unpack(data) end,
        onSave = function() return {sets, index} end,
    }
}

