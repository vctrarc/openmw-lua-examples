if require('openmw.core').API_REVISION < 10 then
    error('This mod requires a newer version of OpenMW, please update.')
end

local self = require('openmw.self')
local util = require('openmw.util')
local camera = require('openmw.camera')

local effectDuration = 60

local counter = 0
local effectStrength = 0

local drunkMovement = util.vector2(0, 0)
local drunkMovementTarget = util.vector2(0, 0)

local function onUpdate(dt)
    if counter <= 0 then return end
    counter = counter - dt
    local progress = 1 - counter / effectDuration
    if progress < 0.3 then
        effectStrength = math.max(effectStrength, progress / 0.3)
    else
        effectStrength = math.min(1, 2 - progress * 2)
    end
    if (drunkMovement - drunkMovementTarget):length() < 0.1 then
        drunkMovementTarget = util.vector2(math.random() * 2 - 1, math.random() * 2 - 1)
    end
    drunkMovement = drunkMovement * (1-dt/2) + drunkMovementTarget * dt/2
    coef = util.vector2(self.controls.sideMovement, self.controls.movement):length() * effectStrength * 0.75
    self.controls.movement = self.controls.movement + drunkMovement.y * coef
    self.controls.sideMovement = self.controls.sideMovement + drunkMovement.x * coef
    if camera.getMode() == camera.MODE.FirstPerson then
        camera.setExtraPitch(camera.getExtraPitch() - drunkMovement.y * effectStrength * math.rad(30))
        camera.setExtraYaw(camera.getExtraYaw() - drunkMovement.x * effectStrength * math.rad(15))
        camera.setRoll(camera.getRoll() - drunkMovement.x * effectStrength * math.rad(30))
    else
        camera.setExtraPitch(camera.getExtraPitch() + drunkMovement.y * effectStrength * math.rad(30))
        camera.setExtraYaw(camera.getExtraYaw() + drunkMovement.x * effectStrength * math.rad(15))
        camera.setRoll(camera.getRoll() + drunkMovement.x * effectStrength * math.rad(30))
    end
end

return {
    engineHandlers = {
        onUpdate = onUpdate,
        onLoad = function(data)
            if data and data.counter then
                counter = data.counter
            else
                counter = 0
            end
        end,
        onSave = function() return {counter = counter} end,
        onConsume = function(recordId)
            if recordId == 'potion_skooma_01' then
                print('Skooma consumed')
                counter = effectDuration
            end
        end,
    },
}

