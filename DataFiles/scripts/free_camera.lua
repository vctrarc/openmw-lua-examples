if require('openmw.core').API_REVISION < 17 then
    error('This mod requires a newer version of OpenMW, please update.')
end

local camera = require('openmw.camera')
local input = require('openmw.input')
local ui = require('openmw.ui')
local player = require('openmw.self')
local util = require('openmw.util')

local start_key = input.KEY['O'] --START FREE CAM MODE HOTKEY

local Pressed = input.isActionPressed
local Action = input.ACTION

local just_started = false
local savedMode = nil
local controlPlayer = false
local height = 0
local target_height = 0
local speed = 150
local acceleration = 1.5
local deceleration = 5
local X, Y = 0,0

local function set_height(v)
    target_height = v
    if v==0 then height = 0 end
end

local function bind_player( v)
    input.setControlSwitch(input.CONTROL_SWITCH.Controls, v)
    input.setControlSwitch(input.CONTROL_SWITCH.Jumping,  v)
end

return {
    engineHandlers = {
        onInputUpdate = function(dt)
            local start = input.isKeyPressed(start_key)
            if start and not just_started then
                bind_player(controlPlayer or (not input.getControlSwitch(input.CONTROL_SWITCH.Controls))) --bind player by Control switch to disable activation of objects by the camera.
                if savedMode then
                    camera.setMode(savedMode)
                    savedMode = nil
                    controlPlayer = false
                    ui.showMessage('Free camera is off')
                else
                    savedMode = camera.getMode()
                    if savedMode == camera.MODE.Static then
                        savedMode = nil
                    else
                        camera.setMode(camera.MODE.Static)
                        ui.showMessage('Free camera is on')
                    end
                end
            end
            just_started = start
            
            if camera.getMode() == camera.MODE.Static and savedMode then
                camera.showCrosshair(false)
                camera.setExtraPitch(0)
                camera.setExtraYaw(0)
                camera.setRoll(0)
                if not controlPlayer then
        
                    player.controls.yawChange = 0
                    local pitch = camera.getPitch()
                    local Z = 0
        
                    if Pressed(Action.MoveForward) then
                        Y = util.clamp(Y+acceleration*dt,0,1)
                        Z = pitch
                    elseif Pressed(Action.MoveBackward) then
                        Y=util.clamp(Y-acceleration*dt,-1,0)
                        Z = -pitch
                    else
                        Y = math.abs(Y)<0.05 and 0 or Y - Y*deceleration*dt
                    end

                    if Pressed(Action.MoveRight) then
                        X=util.clamp(X+acceleration*dt,0,1)
                    elseif Pressed(Action.MoveLeft) then
                        X=util.clamp(X-acceleration*dt,-1,0)
                    else
                        X = math.abs(X)<0.05 and 0 or X - X*deceleration*dt
                    end
                    
                    if target_height ~= height then
                        height = height + (target_height-height)*deceleration*dt
                        if math.abs(target_height-height)<0.05 then
                            set_height(0)
                        else
                            Z = height
                        end
                    end
                    local mx = speed*(input.isKeyPressed(input.KEY.LeftShift) and 4 or 1) --ACCELERATE!
                    local offset = util.transform.rotateZ( camera.getYaw())*util.vector3(X*mx*dt, Y*mx*dt, -Z*mx*dt)
        
                    camera.setPitch(pitch + input.getMouseMoveY() * 0.005)
                    camera.setYaw(camera.getYaw() + input.getMouseMoveX() * 0.005)
                    camera.setStaticPosition(camera.getPosition() + offset)
                end
            end
        end,
        onInputAction = function(action)
            if not savedMode then return end
            if action == Action.TogglePOV then
                controlPlayer = not controlPlayer
                bind_player(controlPlayer)
                ui.showMessage(string.format('Free camera: %s movement', (controlPlayer and 'player' or 'camera')))
            end
            if not controlPlayer then
                if action == Action.ZoomIn then
                    set_height(target_height>=height and target_height + 1 or 0)
                elseif action == Action.ZoomOut then
                    set_height(target_height<=height and target_height - 1 or 0)
                end
            end
        end,
    }
}


